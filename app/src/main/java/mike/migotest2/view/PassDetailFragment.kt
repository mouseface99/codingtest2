package mike.migotest2.view

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.android.synthetic.main.view_pass_list_item.view.*
import mike.migotest2.DateFormatter
import mike.migotest2.R
import mike.migotest2.entity.PassItem
import mike.migotest2.entity.PassStatus
import java.util.*

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class PassDetailFragment : Fragment() {
    private val args: PassDetailFragmentArgs by navArgs()
    private val viewModel: PassViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_detail, container, false)

        viewModel.passItem.observe(viewLifecycleOwner, Observer {
            updateData(it)
        })

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.fetchPassData(args.passId)
    }

    private fun updateData(item: PassItem){
        tv_title.text = item.title
        tv_create_time.text = DateFormatter.format(Date(item.createTime))

        val status = item.getStatus()
        tv_status.text = status.toString()
        tv_status.setTextColor( when(status){
            PassStatus.CREATED -> Color.BLACK
            PassStatus.ACTIVATED -> Color.BLUE
            PassStatus.EXPIRED -> Color.RED
        })
        val naText = getString(R.string.no_data)

        when(status){
            PassStatus.CREATED -> {
                btn_activate.isEnabled = true
                tv_activate_time.text = naText
                tv_expire_time.text = naText
                tv_expire_time.setTextColor(Color.DKGRAY)
            }
            else -> {
                btn_activate.isEnabled = false
                tv_activate_time.text = DateFormatter.format(Date(item.activateTime))
                tv_expire_time.text = DateFormatter.format(Date(item.activateTime + item.period))
                tv_expire_time.setTextColor(Color.RED)
            }
        }

        btn_activate.setOnClickListener {
            viewModel.activateItem(item)
        }
    }
}