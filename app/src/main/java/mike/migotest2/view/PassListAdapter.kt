package mike.migotest2.view

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.android.synthetic.main.view_pass_list_item.view.*
import mike.migotest2.R
import mike.migotest2.entity.PassItem
import mike.migotest2.entity.PassStatus

class PassListAdapter(
    private var inflater: LayoutInflater,
    private var container: ViewGroup?,
    private var passClickListener: (PassItem) -> Unit,
    private var passActivateListener: (PassItem) -> Unit
): ListAdapter<PassItem, PassViewHolder>(PassDiffCallback){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PassViewHolder {
        var view = inflater.inflate(R.layout.view_pass_list_item, container, false)
        return PassViewHolder(view, passClickListener, passActivateListener)
    }

    override fun onBindViewHolder(holder: PassViewHolder, position: Int) = holder.setData(getItem(position))
}

class PassViewHolder(
    var view: View,
    var clickListener: (PassItem) -> Unit,
    var activateListener: (PassItem) -> Unit
) : RecyclerView.ViewHolder(view){

    fun setData(data: PassItem){
        view.tv_title.text = data.title
        view.tv_status.text = data.getStatus().toString()

        view.tv_status.setTextColor( when(data.getStatus()){
            PassStatus.CREATED -> Color.LTGRAY
            PassStatus.ACTIVATED -> Color.BLUE
            PassStatus.EXPIRED -> Color.RED
        })

        view.btn_activate.isEnabled = when(data.getStatus()){
            PassStatus.CREATED -> true
            else -> false
        }
        view.setOnClickListener { clickListener.invoke(data) }
        view.btn_activate.setOnClickListener { activateListener.invoke(data) }
    }
}

object PassDiffCallback : DiffUtil.ItemCallback<PassItem>() {
    override fun areItemsTheSame(oldItem: PassItem, newItem: PassItem): Boolean {
        return (oldItem.id == newItem.id) && (oldItem.activateTime == newItem.activateTime)
    }

    override fun areContentsTheSame(oldItem: PassItem, newItem: PassItem): Boolean {
        return (oldItem.id == newItem.id) && (oldItem.activateTime == newItem.activateTime)
    }
}