package mike.migotest2.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.android.synthetic.main.fragment_list.view.*
import kotlinx.android.synthetic.main.view_input_dialog.view.*
import mike.migotest2.R
import mike.migotest2.dayToMS
import mike.migotest2.entity.PassItem
import mike.migotest2.hoursToMS


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class PassListFragment : Fragment() {
    private val viewModel: PassViewModel by viewModels()

    private var isFABOpen = false
    private lateinit var adapter: PassListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_list, container, false)

        adapter = PassListAdapter(inflater, container,
            { // when clicked view item -> go to detail page
                val action = PassListFragmentDirections.actionListToDetail(it.id)
                findNavController().navigate(action)
            },{// when clicked Activated button -> Activate current pass
                viewModel.activateItem(it)
            }
        )
        rootView.rv_pass_list.adapter = adapter

        viewModel.passList.observe(viewLifecycleOwner, Observer {
            swipe_container.isRefreshing = false
            adapter.submitList(it)
        })

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fab.setOnClickListener {
            if(!isFABOpen){
                showFABMenu()
            }else{
                closeFABMenu()
            }
        }

        fab_day.setOnClickListener { showDialog(true) }

        fab_hour.setOnClickListener { showDialog(false) }

        swipe_container.setOnRefreshListener { viewModel.fetchPassList() }

        viewModel.fetchPassList()
    }

    private fun showDialog(isDay: Boolean){
        closeFABMenu()
        val view = layoutInflater.inflate(R.layout.view_input_dialog, null)
        val editText = view.edit_num
        val text = getString( if(isDay) R.string.title_day else R.string.title_hour)
        view.tv_title.text = getString(R.string.dialog_title, text)
        view.tv_unit.text = text
        AlertDialog.Builder(requireActivity())
            .setView(view)
            .setPositiveButton(R.string.create) { _, _ ->
                val num = editText.text.toString().toInt()
                viewModel.addNewPass(PassItem(
                    title = resources.getQuantityString(
                        if(isDay) R.plurals.pass_day_title else R.plurals.pass_hour_title,
                        num, num),
                    createTime = System.currentTimeMillis(),
                    period = if(isDay) num.dayToMS() else num.hoursToMS()
                ))
            }
            .setNegativeButton(R.string.cancel) { dialog, _ ->
                dialog.dismiss()
            }
            .create().show()
    }

    private fun showFABMenu() {
        isFABOpen = true
        fab_day.animate().apply {
            duration = 250
            startDelay = 0
            translationY(-resources.getDimension(R.dimen.shift_y_1))
        }
        tv_day.animate().apply {
            duration = 250
            startDelay = 0
            alpha(100F)
            translationY(-resources.getDimension(R.dimen.shift_y_1))
        }

        fab_hour.animate().apply {
            duration = 500
            translationY(-resources.getDimension(R.dimen.shift_y_2))
        }
        tv_hour.animate().apply {
            duration = 500
            alpha(100F)
            translationY(-resources.getDimension(R.dimen.shift_y_2))
        }

        fab.animate().apply {
            duration = 250
            rotation(45F)
            scaleX(0.8F)
            scaleY(0.8F)
        }
    }

    private fun closeFABMenu() {
        isFABOpen = false
        fab_day.animate().apply {
            startDelay = 250
            translationY(0F)
        }
        tv_day.animate().apply {
            startDelay = 250
            translationY(0F)
            alpha(0F)
        }
        fab_hour.animate().translationY(0F)
        tv_hour.animate().translationY(0F).alpha(0F)
        fab.animate().rotation(0F).scaleX(1F).scaleY(1F)
    }
}