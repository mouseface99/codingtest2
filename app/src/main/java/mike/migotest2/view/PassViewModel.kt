package mike.migotest2.view

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import mike.migotest2.entity.PassItem
import mike.migotest2.repo.Repository

class PassViewModel(application: Application): AndroidViewModel(application){

    val passList by lazy { MutableLiveData<List<PassItem>>() }
    val passItem by lazy { MutableLiveData<PassItem>() }

    fun fetchPassList(){
        viewModelScope.launch(Dispatchers.IO){
            val list = Repository.getPassRepo(getApplication()).getPassList()
            passList.postValue(list)
        }
    }

    fun fetchPassData(passID: Int){
        viewModelScope.launch(Dispatchers.IO){
            val item = Repository.getPassRepo(getApplication()).getPassItem(passID)
            passItem.postValue(item)
        }
    }

    fun addNewPass(newItem: PassItem){
        viewModelScope.launch(Dispatchers.IO){
            Repository.getPassRepo(getApplication()).addPassItem(newItem)
            fetchPassList() // refresh list
        }
    }

    fun activateItem(item: PassItem){
        viewModelScope.launch(Dispatchers.IO){
            item.activateTime = System.currentTimeMillis()
            Repository.getPassRepo(getApplication()).updatePassItem(item.id, item)
            fetchPassList() // refresh list
            fetchPassData(item.id) // refresh detail page
        }
    }
}