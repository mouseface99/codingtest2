package mike.migotest2

import java.text.SimpleDateFormat

val DateFormatter = SimpleDateFormat("yyyy/MM/dd HH:mm")

fun Int.dayToMS(): Long {
    return this.toLong() * 24 * 60 * 60 * 1000
}

fun Int.hoursToMS(): Long {
    return this.toLong() * 60 * 60 * 1000
}
