package mike.migotest2.repo

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import mike.migotest2.entity.PassItem

@Entity(tableName = "pass_table")
data class PassEntity(
    @PrimaryKey(autoGenerate = true) val id: Int?,
    val title: String,
    @ColumnInfo(name = "create_time") val createTime: Long,
    @ColumnInfo(name = "activate_time") val activateTime: Long,
    val period: Long
) {
    fun toPassItem(): PassItem {
        return PassItem(id!!, title, createTime, activateTime, period)
    }

    companion object {
        fun convert(item: PassItem): PassEntity {
            val id = if (item.id == -1) null else item.id
            return PassEntity(
                id, item.title, item.createTime, item.activateTime, item.period
            )
        }
    }
}