package mike.migotest2.repo

import android.content.Context
import androidx.room.*

@Database(entities = [PassEntity::class], version = 1, exportSchema = false)
abstract class MyDatabase: RoomDatabase(){
    abstract fun getRoomDao(): RoomDao

    companion object {
        @Volatile
        private var INSTANCE: MyDatabase? = null
        fun getDatabase(context: Context): MyDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    MyDatabase::class.java,
                    "pass-data"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}

@Dao
interface RoomDao {
    // Job
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrUpdatePass(pass: PassEntity)

    @Query("SELECT * from pass_table ORDER by id desc")
    fun getPassList(): List<PassEntity>

    @Query("SELECT * from pass_table where id == :passID")
    fun getPassData(passID: Int): PassEntity
}