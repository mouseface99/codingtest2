package mike.migotest2.repo

import mike.migotest2.entity.PassItem

class PassRepo(var roomDao: RoomDao) {

    fun getPassList(): List<PassItem>{
        return roomDao.getPassList().map {
            it.toPassItem()
        }
    }

    fun getPassItem(passID: Int): PassItem {
        return roomDao.getPassData(passID).toPassItem()
    }

    fun addPassItem(item: PassItem){
        roomDao.insertOrUpdatePass(PassEntity.convert(item))
    }

    fun updatePassItem(passID: Int, item: PassItem){
        roomDao.insertOrUpdatePass(PassEntity.convert(item))
    }
}