package mike.migotest2.repo

import android.content.Context

object Repository {

    private var passRepo: PassRepo? = null

    fun getPassRepo(ctx: Context): PassRepo{
        if (passRepo == null)
            passRepo = PassRepo(MyDatabase.getDatabase(ctx).getRoomDao())

        return passRepo!!
    }
}