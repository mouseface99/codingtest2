package mike.migotest2.entity

data class PassItem(
    var id: Int = -1,
    var title: String,
    var createTime: Long,
    var activateTime: Long = -1,
    var period: Long
) {
    private fun isActivate(): Boolean {
        return (activateTime > 0) && (activateTime + period) > System.currentTimeMillis()
    }

    fun getStatus(): PassStatus {
        return if(activateTime < 0) {
            PassStatus.CREATED
        }
        else {
            if(isActivate()) PassStatus.ACTIVATED
            else PassStatus.EXPIRED
        }
    }

    fun getRemainTime(): Long {
        return if(isActivate()) {
            (activateTime + period) - System.currentTimeMillis()
        }
        else{
            0
        }
    }
}

enum class PassStatus{
    CREATED,
    ACTIVATED,
    EXPIRED
}