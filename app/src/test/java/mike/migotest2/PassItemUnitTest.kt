package mike.migotest2

import mike.migotest2.entity.PassItem
import mike.migotest2.repo.PassEntity
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class PassItemUnitTest {
    @Test
    fun itemToEntityTest() {
        val item = PassItem(1,
            "TestPass",
            System.currentTimeMillis(),
            -1,
            3000
        )

        val entity = PassEntity.convert(item)

        assert(item.id == entity.id)
        assert(item.title == entity.title)
        assert(item.createTime == entity.createTime)
        assert(item.activateTime == entity.activateTime)
        assert(item.period == entity.period)
    }

    @Test
    fun entityToItemTest() {
        val entity = PassEntity(1,
            "TestPass",
            System.currentTimeMillis(),
            -1,
            3000
        )

        val item = entity.toPassItem()

        assert(item.id == entity.id)
        assert(item.title == entity.title)
        assert(item.createTime == entity.createTime)
        assert(item.activateTime == entity.activateTime)
        assert(item.period == entity.period)
    }

}