package mike.migotest2

import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.isDialog
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import mike.migotest2.entity.PassStatus
import mike.migotest2.view.PassViewHolder
import org.junit.After
import org.junit.Rule
import org.junit.Test
import java.util.*


@LargeTest
class AccessPassTest {
    @Rule
    @JvmField
    var activityActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @After
    fun removeDB() {
        activityActivityTestRule.activity.deleteDatabase("pass-data")

    }

    @Test
    fun createDayPassTest(){
        val days = Random().nextInt(100)

        // Create pass
        createPass(true, days)

        val targetText = activityActivityTestRule.activity.resources.getQuantityString(R.plurals.pass_day_title, days, days)

        // Check title
        onView(withText(targetText))
            .check(matches(isDisplayed()))
    }

    @Test
    fun createHourPassTest(){
        val hours = Random().nextInt(100)

        // Create pass
        createPass(false, hours)

        val targetText = activityActivityTestRule.activity.resources.getQuantityString(R.plurals.pass_hour_title, hours, hours)

        // Check title
        onView(withText(targetText))
            .check(matches(isDisplayed()))
    }

    @Test
    fun passDetailPageTest(){
        val hours = Random().nextInt(100)

        // Create new pass for test
        createPass(false, hours)

        val createTime = DateFormatter.format(Date())
        val targetTitle = activityActivityTestRule.activity.resources.getQuantityString(R.plurals.pass_hour_title, hours, hours)

        // Enter detail page
        onView(withId(R.id.rv_pass_list))
            .perform(RecyclerViewActions.actionOnItemAtPosition<PassViewHolder>(0, click()))

        // Check Title
        onView(withId(R.id.tv_title)).check(matches(withText(targetTitle)))

        // Check create time
        onView(withId(R.id.tv_create_time)).check(matches(withText(createTime)))
    }

    @Test
    fun activatePassTest(){
        val hours = Random().nextInt(100)

        // Create new pass for test
        createPass(false, hours)

        // Enter detail page
        onView(withId(R.id.rv_pass_list))
            .perform(RecyclerViewActions.actionOnItemAtPosition<PassViewHolder>(0, click()))

        // Activate pass
        onView(withId(R.id.btn_activate)).perform(click())
        val activateTime = System.currentTimeMillis()
        val expiredTime = activateTime + (hours * 60 * 60 * 1000)

        // Check Status
        onView(withId(R.id.tv_status)).check(matches(withText(PassStatus.ACTIVATED.toString())))

        // Check Activate time
        onView(withId(R.id.tv_activate_time)).check(matches(withText(DateFormatter.format(Date(activateTime)))))

        // Check create time
        onView(withId(R.id.tv_expire_time)).check(matches(withText(DateFormatter.format(Date(expiredTime)))))
    }

    private fun createPass(isDays: Boolean, value: Int){
        // open menu
        onView(withId(R.id.fab)).perform(click())

        // click ADD Day/Hour Pass
        onView(withId(
            if(isDays)
                R.id.fab_day
            else
                R.id.fab_hour
        )).perform(click())

        // Input value (days or hours)
        onView(withId(R.id.edit_num)).perform(typeText(value.toString()))

        // Press CREATE button
        onView(withText(R.string.create))
            .inRoot(isDialog())
            .check(matches(isDisplayed()))
            .perform(click());
    }
}